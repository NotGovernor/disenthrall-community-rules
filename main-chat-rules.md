# Main Chat Purpose and Rules
This channel on both Matrix and Discord is for general hangout and conversation about current events and normal day to day stuff.

Some normies may have deeper conversations here cause they dont know how to use discord. Be patient with them. Creating new voluntaryists is more important than switching to the appropriate channel.

**No link dropping / spam**. Put that in Content Links. This channel is for discussions.

**No serious debate**. Take that to meat and potatoes.

**No extreme stuff**, nudity, or stuff that might make the normies we're trying to invite here to discuss voluntaryism with feel uncomfortable.

**Be nice**. Some of the best people on the planet are here.

## What Are Rights and Who Gets them (TLDR Version)
This is discussed frequently so I'm putting this definition here:

Rights are mutual reciprocal understandings between, and that should be afforded to, all beings who are or will become apperceptive to both a. advanced conceptualization, and to b. gratification deferral and long term future planning, and should exclude those who do not continue to reciprocate them.

## Links
- [Go Back to Main Disenthrall Server Page](README.md)
