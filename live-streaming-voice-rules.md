# Live Streaming Voice Purpose and Rules
This channel is for exclusive use by Disenthrall staff. You may join without an invitation but should remain muted unless specifically told to unmute. Because there will usually be live streaming or recording happening in these channels there is a zero tolerance policy for problems here. Expect to get kicked or banned swiftly as we will not be able to provide any special attention to you during recording or streaming.

This channel currently only exists on Discord and is only a voice channel.

## Links
- [Go Back to Main Disenthrall Server Page](README.md)
