# Vija Gamez Purpose and Rules
- These channels are for chatting about video games, movies, music, and other entertainment stuff.

- Theyre for relaxation and fun with friends.

- Theyre **not** for philosophy discussion or debate.

- These channels are not for doomers, or long off topic conversations. Use other channels for that.

- The voice channels are for grouping up with others for gaming.

- If a group is already using a channel for gaming **do not** try to hold your own sperate conversations with other people in the same channel as this may make it hard for the people being sweaty tryhards to communicate... Just switch to the other gaming channel.

## Discord vs Matrix
On **Discord**: Keep it G rated. Nothing that the SJWs at Discord would get their panties in a twist about. Same general rules as for [Disney Memes Only](disney-memes-only.md) applies.  
On **Matrix**: Same general rules as for [Memes n Shitp0sts](memes-n-shitposts.md) applies.

## Links
- [Go Back to Main Disenthrall Server Page](README.md)
