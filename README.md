
# Welcome

Welcome to the Disenthrall Discord / Matrix Server where we are shining the light of day on the invisible violence of government!

This server has three purposes:
1. To provide a friendly place to facilitate conversations that expose new people to the ideas of voluntaryism and freedom and to build a community of awesome people around these ideas.

2. To provide an adult place to debate the ideas and philosophy discussed on the Disenthrall platform.

3. To provide a place for those of like mind to chill, hang out, play some games, and create friendships and real world support networks.

[[_TOC_]]

## General Rules
- Do not report anyone or anything to Discord. Report to admins and mods only.

- Do not spam, scam, doxx, distribute malware, or username squat.

- Do not impersonate people. Parody accounts must be marked as such in their bios.

- No one under 16 years old.

- No government employees of any kind including "law" enforcers.

- Commies will be tolerated as long as they remain on their best behavior and may be restricted to the gulag.

- Do not violate US magic spells (laws).

- Do not do anything that might make us interact with any form of law enforcement (gross).

- Each text and voice channel has a specific purpose to keep these cats hearded. Please mind what you are posting where according the rules for each location.

- Dont be rude. The people here are some of the most awesome that you will ever meet on the tubes.

- Arguments ought to be about the ideas, not ad homs -- and that includes calling peoples actions and positions names.

- If you are only here to spam some youtube links with no introduction, discussion, or context post them only in the Content Links or Memez-n-sh1tp0sts channel.

- Any chat or voice channel may be taken over at any time by Disenthrall staff for the purposes of debate, recording, or any other purpose. When joining a channel listen for a second to make sure recording isnt going on before you start talking.

- If you are here to share media, memes, etc of *political vermin* or *commie garbage* post them only in the gulag where they belong... and remember the hierarchy is watching. ;>

If there is too many notifications going on for your liking, you can turn off notifications on a room by room basis on either Discord or Matrix. (For example, only be notified when memes are posted in the memez channel) Just ask people for help on how to do this.

## Channel Specific rules

- [Main Chat](main-chat-rules.md)
- [Meat and Potatoes](meat-and-potatoes-rules.md)
- [Disney Memes Only](disney-memes-only.md)
- [Memez n' Shitp0sts](memes-n-shitposts.md)
- [Content Links](content-links-rules.md)
- [Vija Gamez](vija-gamez-rules.md)
- [Live Streaming Voice](live-streaming-voice-rules.md)
- [Voluntaryists of DFW](VoDFW-rules.md)
- [The Gulag](gulag-rules.md)
- [Pay Your Taxes](pay-your-taxes-rules.md)

## Links
[List of All Disenthrall and Anarchast Platforms](Disenthrall.me/platforms)

[Element App (for connecting to the Matrix server)](https://element.io/)

[Discord Join Link](https://discord.gg/h2zp76v)

## What Does Disenthrall Mean?
![](thrall.png)

![](enthrall.png)

![](disenthrall.png)

## To Contact Us Privately
Email Patrick at Disenthrall Me using the following PGP key:

[Public Key](pub_key.txt)

## Matrix server
We now have a Matrix server for the adults. Keep the content you post on Discord G rated from now on so that it doesnt get deleted by the Marxists's at Discord.

To join the grown ups download the Element client desktop or phone apps. For instructions on connecting watch this video:

[YouTube (gross)](https://www.youtube.com/watch?v=ejhnfhV_-Uk), [Odysee / LBRY](https://odysee.com/@Disenthrall:8/How-to-Install-and-Connect-to-Our-Matrix-Server:f?r=BPhKSZHDyuxNdwW89qeMzBNzgFNJPvTD)

## Server Kicks / Bans
These rules may be updated at any time, check back often. Severe or repeated issues will get you kicked suspended or banned. This is private property. You are not owed due process, though we will try our best with the time we have available.

We dont have a lot of admins -> because we dont need them -> because the people on this server are generally good people that are kind to each other -> because the people that arent get banned -> because we dont have time for drama -> because we dont have a lot of admins. *see how that works? :D*

We have real life productive stuff to do and dont want to have to allocate a bunch of time to moderating a bunch of drama. Mute or block people you dont like. Dont escalate with people breaking the rules, just let us know privately.

We have permanently banned very few people. Most people just get kicked from the server the first time as a wake up call. Chill out for a bit and join again. Read the rules. Agree to them. Welcome back.

There are thousands of other discord / matrix servers that put up with drama. This isnt one of them.

Help make this a better place for Voluntaryists!

## Lead Yourself!
