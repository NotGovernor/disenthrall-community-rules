# Meat and Potatoes Purpose and Rules
This is a **heavily moderated debate space**. Be concise, productive, genuine, polite and respectful. Be on the same side working towards finding truth.

## Agreements to Participate Here:
1. **No name calling** (and that includes calling peoples positions, actions, or behavior names)

2. **Take it to voice** as often as possible. Video chat is even better. Text is one of the worst methods of discourse available.

3. **No trolling**. Engage honestly and earnestly. No tit for tat'ing or taking shots.

4. **Keep debates between two people** at a time. Too many cooks in the kitchen makes it go all over the place and nowhere at the same time. If you have a topic specify who you want to discuss it with or ask for a volunteer. Tap out if you want to let someone else take your spot. If you are not one of the two current debaters and have comments, disagreements, thoughts, or discussion points put them in main chat for people to discuss with you as they watch the debate going on in Meat and Potatoes.

5. Speak or type and **break for replies**. Try to avoid typing your points in multiple short messages that people will have to respond to individually spagettifying the chat and making it hard to follow. If in voice say when you are done talking so the other person knows when they can speak without interrupting you.

6. **Do not interrupt. Do not filibuster**. Do not drone on and on. Make your point concisely and hand it off. Vlog somewhere else. The only exception to interrupting should be asking a question to get a clarification or stopping a straw man in progress. This is not carte blanche to take the mic back and start talking again. Say **"point of clarification"** to get the mic long enough for one or two sentences to ask your question or clarify before letting them continue.

7. **Winning is finding truth** not being right.

8. Logical fallacies are for the commies and other irrationals -- **neither belong here**.

9. Try to tackle **one topic or point of debate at a time**. State or acknowledge when moving on.

10. **No memes or react gifs and images**.

11. **Take conversations that arent debates elsewhere.** This channel is reserved for debates only and rule number 4 may prevent others that want to debate from using it if they see people already active in it.

12. If the bot posts a message reminder to follow these rules that you agreed to **consider it a courtesy final warning** before you lose access to post in the channel.

13. This list may change. **Check it often**.

## How to Get Access to This channel
On **Matrix** access to post in this channel is open to everyone because we havnt had many problems there.  
On **Discord** you must read and show that you understand and agree to follow these rules by reacting to the post in the "welcome-rules" channel.

If you find that you have lost access to this channel it is most likely because you forgot these rules. Its usually not a permanent severe thing; just re-read the rules and contact an admin to request access to the channel again. Repeated problems will of course make your loss of access permanent. Severe problems will likely result in a server ban.

## Definitions and Premises
The following are the basic definitions and presumptions for discussions in this room. Consider these debates "already settled". Some limited debate on these points is fine but it should not dominate the room. If you see a problem or something that should be added contact an admin.

- **Truth**: That which is logically consistent (conforms to rules of reason) and congruent with the evidence (accurate to reality).

- **Logic**: Objective and consistent rules derived from the consistency of reality.

- **Valid**: A position that conforms to logic.

- **Accurate**: A position that conforms to reality.

- **True**: A position that is both valid and accurate.

- **A = A**: Reality is objective and consistent.

- **We exist**.

- Our senses have the **capacity for accuracy**.

- Language has the **capacity for meaning**.

## Grahams Hierarchy of Disagreement
![](Grahams_Hierarchy_of_Disagreement.png)  
Exist primarily around the top of this pyramid and you'll do fine here.

## General Warning After A Series of Frustrating Situations :D
If you break these rules or shit up the place you may be warned once, you may just get banned... You might also just lose the ability to use these channels. We dont owe you due process. Make this server a better more friendly productive place or GTFO.

## Links
- [Go Back to Main Disenthrall Server Page](README.md)
